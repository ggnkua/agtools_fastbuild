; =============================================================================
;	Base build config
; =============================================================================
; make job can specify one AGT 'architecture' to target at a time (mainly STE)
#define STE

; project-relative location of agtools repo root dir
; note: can remove this and set $AGTROOT var externally if preferred
.AGTROOT="../.."
.PROJECT_NAME="map16x2"
.MAIN_PROJECT_SRC=.PROJECT_NAME+".cpp"
.GCCROOT="$AGTROOT$/bin"

; debug/testing/release
;#define BUILD_CFG_DEBUG
;#define BUILD_CFG_TESTING
#define BUILD_CFG_RELEASE

; select debug console - uncomment only one line, or none
;#define USE_DEBUG_CONSOLE_NATIVE
;#define USE_DEBUG_CONSOLE_UDEV

; remote testing post-build step, if required [ultradev,hxcusb]
#define POST_REMOTE_ULTRADEV
.REMOTE_PRG = .PROJECT_NAME+".prg"

; uncomment if you use any packed assets, or files in general
#define PACKED_ASSETS

; =============================================================================
;	Optional build config
; =============================================================================

; if yes, will rebuild colourmaps on changes. otherwise only build if missing
; notes: 
; - colourmap generation can be expensive to run - can take several minutes
; - some generate modes are nondeterministic, not guaranteed same result every time
; - projects with prebuilt colourmaps will be committed with 'no'
;#define REBUILD_COLMAPS

; uncomment to links Hatari natfeats lib into program
;#define USE_HATARI_NATFEATS

; =============================================================================
;	Post-build steps
; =============================================================================

; pack (release) executables, post-build
;#define POST_UPX_PACKER

; =============================================================================
;   AGT engine configuration
; =============================================================================
; Notes:
;     - see Wiki: https://bitbucket.org/d_m_l/agtools/wiki/Compiletime%20Flags
; -----------------------------------------------------------------------------
.COMPILERDEFS = ""
		+" -DAGT_CONFIG_WORLD_XMAJOR"
		+" -DAGT_CONFIG_PACKER_ANY"

; =============================================================================
;   Debugging, diagnostic logging & profiling options
; =============================================================================
; Notes:
;     - see Wiki: https://bitbucket.org/d_m_l/agtools/wiki/Compiletime%20Flags
; -----------------------------------------------------------------------------

.COMPILERDEFS + ''
;		+' -DAGT_CONFIG_SAFETY_CHECKS"
;		+' -DAGT_CONFIG_DEBUG_CONSOLE"
;		+' -DAGT_CONFIG_DEBUGDRAW"
;		+' -DAGT_CONFIG_STEEM_DEBUG"
;		+' -DTIMING_RASTERS -DTIMING_RASTERS_MAIN"
;		+' -DUSE_MEMTRACE"

; Add any project custom flags here
.CFLAGS   = ''
.CXXFLAGS = ''
.LDFLAGS  = ''

; =============================================================================
;   Additional project-local sourcefiles (if any)
; =============================================================================

.PROJECT_SRC_LIST = {}
.PROJECT_INC_LIST = ''

; #include doesn't seem to like variables like $AGTROOT$...
#include "../../makedefs.bff"

;
; Binaries definition
;
.product_disk1 = [ 
    .project_name = "$PROJECT_NAME$"
    .source_file  = .MAIN_PROJECT_SRC
    .output_path  = "disk1"
]
.binaries = { .product_disk1 }

; =============================================================================
;   Project assets
; =============================================================================

; input files which cause palettes to be rebuilt with [REBUILD_PALETTES = yes]
; NOTE: optional, to trigger auto-rebuild of palettes when these change

.PALETTE_SOURCES = ''

; assets common to all stages
.COM_SRC = "source_assets"

; if colourmap is required for asset conversion, specify it here
.COLMAP = ""

.ASSETS_SFX = 
{
}

.ASSETS_MUSIC = 
{
}

.AGTCUT_EXEC_LIST = {}      ; This is filled out by the includes below
.PACK_LIST        = {}      ; Ditto
.ADDITIONAL_COPY_FILES = {} ; Ditto

#include "../../pack.bff"   ; Include packer definitions
#include "assets_com.bff"

; =============================================================================
;   Build machinery
; =============================================================================

#include "../../makerules.bff"

;------------------------------------------------------------------------------
;
; The alias to "all" target (which is default). This will convert assets, build code, link everything, etc
;
Alias( 'all' ) { .Targets = {
    .DISK_TARGETS
#if REBUILD_COLMAPS
    'colourmaps',
#endif
    'pack_assets',
} }

