# Portable build system for Atari Game Tools by dml

Real desrciption and instructions coming up at some point. For now:

1) Clone the AGT repository: https://bitbucket.org/d_m_l/agtools/src/master/

2) Clone this repository

3) Depending on your OS, do one of the following:

  * Windows: Un7zip browncc-12.2.7z into agtools\bin\win

  * Linux: tar -Jxvf linuxbrown-12.2.tar.xz into agtools/bin/Linux/browncc-12.2/

  * Mac x64: tar -Jxvf macbrown-12.2.tar.xz into agtools/bin/Darwin/browncc-12.2/

  * Mac M1: tar -Jxvf macm1brown-12.2.0.tar.xz into agtools/bin/Darwin/browncc-12.2/

4) Copy this folders "demos", "examples" and "tutorials" from this repository to agtools, as well as all the files with *bff* extension

5) Go to one of the demos, examples or tutorials folder

6) Open a command line (cmd.exe for Windows) and type *fbuild* (*./Fbuild-linux* for linux and *./FBuild* for osx). If there is an error you could try *fbuild -j1* or failing that, let us know! 


# Some known open issues:

- `bosscore` currently does not compile

- `isoscroll` produces an executable that is not working

- Packing modes `arj4` and `arj7` are not functioning

- ...plus everything inherited by AGT

- On x64 OSX machines that have v12.0 (Monteray) and beyond, it is high likely you will encounter the following message:

```
dyld[43503]: Library not loaded: '/usr/local/opt/zstd/lib/libzstd.1.dylib'
  Referenced from: '_temp/fbuildagttest/agtools/bin/Darwin/browncc-12.2/libexec/gcc/m68k-atarimegabrowner-elf/12.2.0/cc1plus'
  Reason: tried: '/usr/local/opt/zstd/lib/libzstd.1.dylib' (no such file), '/usr/local/lib/libzstd.1.dylib' (no such file), '/usr/lib/libzstd.1.dylib' (no such file)
```

In this case you can try the following:

1) Try to compile gcc for yourself and replace the pre-built one (https://github.com/ggnkua/bigbrownbuild-git)

2) Run the following script on the command line

```
brew install zstd
sudo ln -s /opt/homebrew/Cellar/zstd/1.5.2/lib/libzstd.1.5.2.dylib /usr/local/lib/libzstd.1.dylib
```

# Credits?

Sure! George Nakos did mostly everything in this repository, so he's to blame. Mark Fechtner very kindly created the Mαc M1 build and provided much feedback and fixes for Mαc in general. Finally (and most importantly) Douglas Little encouraged this experiment to come to its conclusion.

# Relevant links

[The fantastic AGT engine](https://bitbucket.org/d_m_l/agtools/src/master/)

[gcc/g++ with libraries for 68k ELF target for Atari ST and compatibles](https://github.com/ggnkua/bigbrownbuild-git)

[68k ELF binary to TOS converter](https://github.com/ggnkua/brownout-git)

[The fantastic FASTBuild](https://www.fastbuild.org/docs/home.html)
