;==============================================================================
;   Build sprite assets
;==============================================================================

; output dir
.OUT="build"

; location of source assets
.SRC="source_assets"

; dither or dualfield colour translation (from PCS), or ST palette source
.COLMAP="source_assets/xenpal.pi1"

; working directory for AGTCUT/EMX codegen tasks
.GENTEMP="build"

;==============================================================================
;   Map cutting config
;==============================================================================

; we can pass these to all tasks
.MAP_DEFAULTS="-t 0.33 -bp 4 -v -vis"

; composite cutting command for all maps
.MAP="-cm tiles -om direct $MAP_DEFAULTS$ -p $COLMAP$"

;------------------------------------------------------------------------------
;   Map
;------------------------------------------------------------------------------

; cut map & tiles, dual-field
; this also creates build/isoscrl0.cct and build/isoscrl1.cct but is not visible by the rule
.map= [ .name='map' .input='$SRC$/map3.png' .output='build/bosscore.ccm' .args="$MAP$ -o %2 -s %1" ]
.AGTCUT_EXEC_LIST+{.map}

; compress assets with lz77
;
.pack_map_1=[.name='pack_map_1' .file="build/bosscore.ccm" .output="$ASSETS$/bosscore.ccm" .PACKER=.PACK_LZ77 .exclude_list_name='map' ]
.pack_map_2=[.name='pack_map_2' .file="build/bosscore.cct" .output="$ASSETS$/bosscore.cct" .PACKER=.PACK_LZ77 .exclude_list_name='map' ]

.PACK_LIST+{.pack_map_1,.pack_map_2}

;==============================================================================
;   Sprite cutting config
;==============================================================================

; for diagnostics, verbosity etc. #-v -q
.DEBUG="-prv -vis"  

; keycolour, tolerance
.KEYCFG="-keyrgb ff:00:ff -kg -kt 0.01"

; we can pass these to all tasks
.SPR_DEFAULTS="-crn 224 -gm -bld $GENTEMP$"

; default optimization level (lower value for faster cutting, quicker turnaround)
.DEFAULT_OPTI="-emxopt 18,12"

;------------------------------------------------------------------------------
; composite cutting commands for most cases
;------------------------------------------------------------------------------

; composite cutting command for all sprites
.SPR="$SPR_DEFAULTS$ -p $COLMAP$ $KEYCFG$ $DEBUG$ $DEFAULT_OPTI$"
.SLS="-cm slabs -p $COLMAP$ $KEYCFG$ $DEBUG$"
.SLR="-cm slabrestore -p $COLMAP$ $KEYCFG$ $DEBUG$"

;------------------------------------------------------------------------------
;   Sprites
;------------------------------------------------------------------------------
; -nc = no clipping (saves memory)

; boss
.boss_emx=[.name="boss_emx" .output="$OUT$/bosscore.emx" .input="$SRC$/xenboss.png"  .args="$SPR$ -o %2 -s %1 -sxp 0 -syp 0 -sxs 144 -sys 96 -sc 1		-cm emxspr"]
.boss_sls=[.name="boss_sls" .output="$OUT$/bosscore.sls" .input="$SRC$/xenboss.png"  .args="$SLS$ -o %2 -s %1 -sxp 0 -syp 0 -sxs 144 -sys 96 -sc 1"]
.boss_slr=[.name="boss_slr" .output="$OUT$/bosscore.slr" .input="$SRC$/xenboss.png"  .args="$SLR$ -o %2 -s %1 -sxp 0 -syp 0 -sxs 144 -sys 96 -sc 1"]

; boss parts
.bosseye= [.name="bosseye"  .output="$OUT$/bosseye.emx"  .input="$SRC$/bosseye.png"  .args="$SPR$ -o %2 -s %1 -sxp 0 -syp 0 -sxs 22 -sys 16 -sxi 22 -syi 0 -sc 6	-cm emxspr"]
.bosspulp=[.name="bosspulp" .output="$OUT$/bosspulp.emx" .input="$SRC$/bosspulp.png" .args="$SPR$ -o %2 -s %1 -sxp 0 -syp 0 -sxs 30 -sys 8 -sxi 0 -syi 8 -sc 6	-cm emxspr -nc"]
                                                                           
; boss weapons                                                             
.balls=   [.name="balls"    .output="$OUT$/balls.emx"    .input="$SRC$/balls.png"    .args="$SPR$ -o %2 -s %1 -sxp 0 -syp 0 -sxs 10 -sys 10 -sxi 0 -syi 10 -sc 9	-cm emxspr -nc"]
.beam=    [.name="beam"     .output="$OUT$/beam.emx"     .input="$SRC$/sprites.png"  .args="$SPR$ -o %2 -s %1 -sxp 361 -syp 162 -sxs 30 -sys 4 -sc 1			-cm emxspr -nc"]
                                                                           
; player                                                                   
.vicviper=[.name="vicviper" .output="$OUT$/vicviper.emx" .input="$SRC$/player.png"   .args="$SPR$ -o %2 -s %1 -sxp 46 -syp 2 -sxs 23 -sys 16 -syi 18 -sc 9		-cm emxspr"]
.bullet=  [.name="bullet"   .output="$OUT$/bullet.emx"   .input="$SRC$/sprites.png"  .args="$SPR$ -o %2 -s %1 -sxp 172 -syp 28 -sxs 9 -sys 2 -sxi 12 -sc 2		-cm emxspr -nc"]

; compress assets with lz77
;

; TODO: NOTES: a) when declaring packed files, do not declare the output packed filename same as source unpacked
.pack_boss_emx=[.name="boss_emx" .file="$OUT$/bosscore.emx" .output="$ASSETS$/bosscore.emx" .PACKER=.PACK_LZ77 .exclude_list_name="boss_emx"]
.pack_boss_sls=[.name="boss_sls" .file="$OUT$/bosscore.sls" .output="$ASSETS$/bosscore.sls" .PACKER=.PACK_LZ77 .exclude_list_name="boss_sls"]
.pack_boss_slr=[.name="boss_slr" .file="$OUT$/bosscore.slr" .output="$ASSETS$/bosscore.slr" .PACKER=.PACK_LZ77 .exclude_list_name="boss_slr"]
.pack_bosseye =[.name="bosseye"  .file="$OUT$/bosseye.emx"  .output="$ASSETS$/bosseye.emx"  .PACKER=.PACK_LZ77 .exclude_list_name="bosseye" ]
.pack_bosspulp=[.name="bosspulp" .file="$OUT$/bosspulp.emx" .output="$ASSETS$/bosspulp.emx" .PACKER=.PACK_LZ77 .exclude_list_name="bosspulp"]
.pack_balls   =[.name="balls"    .file="$OUT$/balls.emx"    .output="$ASSETS$/balls.emx"    .PACKER=.PACK_LZ77 .exclude_list_name="balls"   ]
.pack_beam    =[.name="beam"     .file="$OUT$/beam.emx"     .output="$ASSETS$/beam.emx"     .PACKER=.PACK_LZ77 .exclude_list_name="beam"    ]
.pack_vicviper=[.name="vicviper" .file="$OUT$/vicviper.emx" .output="$ASSETS$/vicviper.emx" .PACKER=.PACK_LZ77 .exclude_list_name="vicviper"]
.pack_bullet  =[.name="bullet"   .file="$OUT$/bullet.emx"   .output="$ASSETS$/bullet.emx"   .PACKER=.PACK_LZ77 .exclude_list_name="bullet"  ]

.AGTCUT_EXEC_LIST + { .boss_emx, .boss_sls, .boss_slr, .bosseye, .bosspulp, .balls, .beam, .vicviper, .bullet }
.PACK_LIST + { .pack_boss_emx, .pack_boss_sls, .pack_boss_slr, .pack_bosseye, .pack_bosspulp, .pack_balls, .pack_beam, .pack_vicviper, .pack_bullet }
