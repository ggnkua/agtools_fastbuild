;==============================================================================
;   Build sprite assets
;==============================================================================

; output dir
.OUT="assets"

; location of source assets
.SRC="source_assets"

; dither or dualfield colour translation (from PCS), or ST palette source
.COLMAP="source_assets/chunky8.ccs"

; working directory for AGTCUT/EMX codegen tasks
.GENTEMP="build"

;==============================================================================
;   Sprite cutting config
;==============================================================================

; for diagnostics, verbosity etc. #-v -q
.DEBUG="-prv -vis"  

; keycolour, tolerance
.KEYCFG="-keyrgb ff:00:ff -kg -kt 0.01"

; we can pass these to all tasks
.SPR_DEFAULTS="-gm -bld $GENTEMP$"

; default optimization level (lower value for faster cutting, quicker turnaround)
.DEFAULT_OPTI=""
.OPTI="-emxopt 20"

;------------------------------------------------------------------------------
; composite cutting commands for most cases
;------------------------------------------------------------------------------

; composite cutting command for all sprites
.SPR="$SPR_DEFAULTS$ -p $COLMAP$ $KEYCFG$ $DEBUG$ $DEFAULT_OPTI$"


;------------------------------------------------------------------------------
;   Sprites
;------------------------------------------------------------------------------
; -nc = no clipping (saves memory)


; vicviper/player
.vicviper=[.name='vicviper' .output='$OUT$/vicviper.emx'  .input='$SRC$/player.png' .args="$SPR$ -o %2 -s %1 -sxp 46 -syp 2 -sxs 23 -sys 16 -syi 18 -sc 9      -cm emxspr"]

; option (multiple)
.options= [.name='options'  .output='$OUT$/options.emx'   .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 309 -syp 23 -sxs 16 -sys 12 -sxi 17 -sc 3     -cm emxspr -ccr low"]

; lasers
.lasers=  [.name='lasers'   .output='$OUT$/lasers.emx'    .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 216 -syp 19 -sxs 32 -sys 1 -sxi 0 -sc 1      -cm emxspr -ccr low  -nc"]
.dlasers= [.name='dlasers'  .output='$OUT$/dlasers.emx'   .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 216 -syp 23 -sxs 32 -sys 3 -sxi 0 -sc 1      -cm emxspr -ccr low  -nc"]
.plasers= [.name='plasers'  .output='$OUT$/plasers.emx'   .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 216 -syp 27 -sxs 14 -sys 5 -sxi 0 -sc 1      -cm emxspr -ccr high  -nc"]

; flame (really, volcano lavabomb)
.flames=  [.name='flames'   .output='$OUT$/flames.emx'    .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 220 -syp 124 -sxs 16 -sys 16 -sxi 16 -sc 3   -cm emxspr"]

; powerup drone
.powerup= [.name='powerup'  .output='$OUT$/powerup.emx'   .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 156 -syp 44 -sxs 16 -sys 14 -sxi 19 -sc 8    -cm emxspr  -nc"]

; bullet #1
.bullet=  [.name='bullet'   .output='$OUT$/bullet.emx'    .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 172 -syp 28 -sxs 9 -sys 2 -sxi 12 -sc 2      -cm emxspr  -nc"]

; bullet #2
.bulletd= [.name='bulletd'  .output='$OUT$/bulletd.emx'   .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 196 -syp 26 -sxs 7 -sys 7 -sxi 10 -sc 2      -cm emxspr  -nc"]

; missile
.missile= [.name='missile'  .output='$OUT$/missile.emx'   .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 311 -syp 45 -sxs 12 -sys 12 -sxi 13 -sc 5    -cm emxspr  -nc"]

; pink splat
.sparks=  [.name='sparks'   .output='$OUT$/sparks.emx'    .input='$SRC$/sprites.png' .args="$SPR$ -o %2 -s %1 -sxp 228 -syp 451 -sxs 16 -sys 16 -sxi 19 -sc 5   -cm emxspr  -nc"]

.AGTCUT_EXEC_LIST + { .vicviper, .options, .lasers, .dlasers, .plasers, .flames, .powerup, .bullet, .bulletd, .missile, .sparks }
